#' make a genome browser like plot, used for both chromatin states and transcription factors.
#' @param Chr the chromosome number.
#' @param topHits the LD block from the aimSNiPER list
#' @param gr.data a genomicRanges object with chromatin data or transcription factor data.
#' @param TFs logical. if TRUE make transcription factor plot, else make chromatin plot.
getChromatinPlot <- function(Chr, topHits, gr.data, TFs=TRUE) {
  if (nrow(topHits)>1) {
    #make GRanges object of LD range
    SNPsrange <- GenomicRanges::GRanges(
      seqnames = paste0("chr", Chr),
      ranges = IRanges::IRanges(start = min(topHits$start),
                                end = max(topHits$start))
    )} else {
      SNPsrange <- GenomicRanges::GRanges(
        seqnames = paste0("chr", Chr),
        ranges = IRanges::IRanges(start = topHits$start-10000,
                                  end = topHits$start+10000)
      )}
  #find TFs overlapping LD range (or chromatin state instead of TFs)
  TFsonRange <- GenomicRanges::findOverlaps(gr.data, SNPsrange)
  TFsonRange <- gr.data[TFsonRange@from,]
  TFsonRange$sample <- as.factor(droplevels(TFsonRange$sample))
  #change TFsonRange in dataframe to use in ggplot
  rangePlotData <- GenomicRanges::as.data.frame(TFsonRange)

  if (TFs) {
    #no groups in Transcription factor data so y is numeric of sample
    rangePlotData$new.y <- as.numeric(rangePlotData$sample)
  } else {
    rangePlotData <- rangePlotData[order(rangePlotData$group),]
    res.o <-lapply(unique(rangePlotData$group), getNewY,
                   Data=rangePlotData)
    rangePlotData$new.y <- do.call(c, res.o)
  }
  #named vector for making the colors
  fillScale <- c("Heterochromatin"="#8a91d0", "Quiescent/Low"="#ccccbf",
                 "Active TSS"="#ff0000", "Weak transcription"="#006400",
                 "ZNF genes & repeats"="#66cdaa", "Enhancers"="#ffff00",
                 "Flanking Active TSS"="#ff4500",
                 "Weak Repressed PolyComb"="#c0c0c0",
                 "Repressed PolyComb"="#808080", "Bivalent Enhancer"="#bdb76b",
                 "Bivalent/Poised TSS"="#cd5c5c",
                 "Flanking Bivalent TSS/Enh"="#e9967a",
                 "Strong transcription"="#008000", "Genic enhancers"="#c2e105",
                 "Transcr. at gene 5' and 3'"="#32cd32")

  RegionsPlot <- ggplot2::ggplot(data = rangePlotData,
                                 ggplot2::aes(x=start,
                                              y=sample)) +
    ggplot2::geom_segment(ggplot2::aes(x=-Inf,xend=Inf,
                                       y=sample, yend=sample),
                          linetype=3, color="black") + #dotted guide lines for tissue titles
    ggplot2::geom_rect(ggplot2::aes(fill=target,
                                    xmin=rangePlotData$start,
                                    xmax=rangePlotData$end),
                       ymin=rangePlotData$new.y-0.4,
                       ymax=rangePlotData$new.y+0.4,
                       alpha=0.7) +
    ggplot2::geom_segment(data = topHits,
                          ggplot2::aes(x=start, xend=start,
                                       y=-Inf,
                                       yend=Inf),
                          linetype=6, alpha=0.3, color="black")  #dashed vertical SNP line
  ggplot2::theme(axis.title = ggplot2::element_blank())
  if (TFs) { #transcription factor specific settings
    RegionsPlot <- RegionsPlot +
      ggplot2::guides(fill=ggplot2::guide_legend(title="Transc.factor"))
  } else { #chromatin state specific settings
    RegionsPlot <- RegionsPlot +
      ggplot2::scale_fill_manual(values = fillScale) +
      ggplot2::facet_grid(group ~ ., scales = "free_y", space = "free_y") +
      ggplot2::theme(strip.text.y = ggplot2::element_text(angle = 0),
                     legend.position = 'none')
  }
  return(RegionsPlot)
}
