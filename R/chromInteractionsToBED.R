chromInteractionsToBED <- function(interactions, rangeOfInterest) {
  #split interactions GRanges into 2 bed format dataframes
  #GRanges to dataframe
  df <- GenomicRanges::as.data.frame(interactions)
  #make dataframe of region to plot
  region <- data.frame(chr=rangeOfInterest@seqnames[1],
                       start=min(c(df$start, df$to.start,
                                   GenomicRanges::start(rangeOfInterest))),
                       end=max(c(df$end, df$to.end,
                                 GenomicRanges::end(rangeOfInterest))))
  #make bed format dataframe of from ranges
  from <- data.frame(chr=df$seqnames, start=df$start, end=df$end,
                     gene=df$from.gene, tissue=df$cell.tissue, id=1:nrow(df))
  #make bed format dataframe of to ranges
  to <- data.frame(chr=df$to.seqnames, start=df$to.start, end=df$to.end,
                   gene=df$to.gene, id=1:nrow(df))
  #format chr columns
  from$chr <- as.character(from$chr)
  to$chr <- as.character(to$chr)
  #return dataframes in a list
  return(list(from, to, region))
}
