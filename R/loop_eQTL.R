loop_eQTL <- function(genesx, lead)
{
  all.comb <- .getComb(genes=genesx, variants=lead)
  x <- POST("https://gtexportal.org/rest/v1/association/dyneqtl",
            encode = "json",
            body = all.comb
  )
  a <- httr::content(x, "parsed")
  a <- a[[2]]

  res <- lapply(1:length(a), FUN=function(i){
    temp <- a[[i]]
    nully <- sapply(1:length(temp), FUN = function(x){is.null(temp[[x]])})
    temp[nully] <- NA
    a[[i]] <- do.call(cbind, temp)
  })
  outdata <- do.call(rbind, res)
  outdata <- as.data.frame(outdata, stringsAsFactors = F)
  outdata$pValue <- as.numeric(outdata$pValue)
  outdata$pValueThreshold <- as.numeric(outdata$pValueThreshold)
  #outdata$Sign <- ifelse(outdata$pValue <= outdata$pValueThreshold,1,0)
  outdata <- outdata[!is.na(outdata$pValueThreshold),]

}
