getNewY <- function(tissue, Data) {
  #get a new y value for every group, needed for geom_rect when faceting
  groupData <- Data[is.element(Data$group, tissue),]#subset group
  new.y <- as.character(groupData$sample)#get subgroups
  new.y <- as.numeric(factor(new.y))#make new y values in subgroup
  return(new.y)
}


