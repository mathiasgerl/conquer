#' Create RDS files with data for multiple SNPs. returns a summary.
#' @param variants a character vector with the rs numbers of the SNPs of interest
#' @param directory a string with the path of a directory for the generated data
#' @param population a string with the population for getting the LD values. Default is "CEU".
#' @return a dataframe with eQTLs for the given SNPs
#' @export
summarise <- function(variants, directory, token, population="CEU") {
  if (!dir.exists(directory)) { #create dir if it doesn't exist
    dir.create(directory)
  }

  # Store LD SNPs
  SNPsInLD <- character()

  for (variant in variants) {
    #check if given variants are not in LD (prevents duplicate data)
    if (!variant %in% SNPsInLD) {
      paste("variant:", variant) %>% message()

      filename <- paste0(directory, "/SNiPER.", population,
                         ".", variant, ".rds")
      #check if file exists already (not creating a snp twice)
      if (!file.exists(filename)) {
        #if SNiPERdata not in global variables, load SNiPER
        if (!exists("SNiPERdata", envir = .GlobalEnv)) {
          loadSNiPER()
        }

        pb <- dplyr::progress_estimated(13, min_time = 2)

        #pb
        pb$tick()$print()

        #get SNPs
        mainSNP <- getVariantInfoEnsembl(variant,
                                         paste0("1000GENOMES:phase_3:", population))
        LD <- getLDLink(SNP = mainSNP, token = token, pop = population)
        topHits <- getTopHits(LD, mainSNP$variation)
        SNPsInLD <- c(SNPsInLD, topHits$variation)
        #chromosome interaction plot data
        chromIntData <- makeChromIntData(mainSNP$chr, topHits, SNiPERdata$chromInt)

        #pb
        pb$tick()$print()

        if (is.null(chromIntData)) { #if there is no chrom int data
          geneStart <- ifelse(as.integer(mainSNP$start)-500000<0, 0,
                              as.integer(mainSNP$start)-500000)
          geneEnd <- as.integer(mainSNP$start)+500000
        } else {
          #use chromosome interaction data to get region for genes (minimum 1Mb region)
          geneStart <- chromIntData[[3]]$start
          geneEnd <- chromIntData[[3]]$end
          #if start 1Mb window below zero, make it zero
          oneMbLimit <- ifelse(as.integer(mainSNP$start)-500000<0, 0,
                               as.integer(mainSNP$start)-500000)
          geneStart <- ifelse(geneStart<oneMbLimit, geneStart, oneMbLimit)
          #if chromosome interaction region ends before 1Mb window, use 1mb window end
          geneEnd <- ifelse(geneEnd>(as.integer(mainSNP$start)+500000),
                            geneEnd, (as.integer(mainSNP$start)+500000))
        }

        #pb
        pb$tick()$print()

        Genes <- getGenesInRegion(chr = mainSNP$chr,
                                  startPos = geneStart,
                                  stopPos = geneEnd)

        locuszoomGenes <- Genesubset(Genes, min(LD$start), max(LD$start))
        Exon <- getExons(locuszoomGenes)

        #make a subset of the chromatin data (for faster plot making)

        #pb
        pb$tick()$print()

        ChromatinData <- SNiPERdata$Chromatin[SNiPERdata$Chromatin@seqnames==
                                              paste0("chr", mainSNP$chr),]

        #make TF plot
        TFdata <- SNiPERdata$TFall[SNiPERdata$TFall@seqnames==paste0("chr",
                                                                 mainSNP$chr),]
        TFdata <- Genesubset(TFdata,
                             min(topHits$start)-10000, max(topHits$start)+10000)

        #pb
        pb$tick()$print()

        #pb
        pb$tick()$print()

        #get SNPs in LD from pQTL dataset
        pQTLs <- SNiPERdata$pQTL[is.element(SNiPERdata$pQTL$rsID,
                                          topHits$variation),]

        #pb
        pb$tick()$print()

        #experimental miQTLs
        exp.miQTLs <- SNiPERdata$miQTLexp[is.element(SNiPERdata$miQTLexp$SNP,
                                                   topHits$variation),]

        #pb
        pb$tick()$print()

        #predicted miQTLs
        pred.miQTLs <- SNiPERdata$miQTLpred[is.element(SNiPERdata$miQTLpred$SNP,
                                                       topHits$variation),
                                            c(1,2,3,4,7)]

        #pb
        pb$tick()$print()

        #cis eQTLs
        cisGenes <- Genesubset(Genes,
                                ifelse(as.integer(mainSNP$start)-500000<0, 0,
                                       as.integer(mainSNP$start)-500000),
                                (as.integer(mainSNP$start)+500000))


        #pb
        pb$tick()$print()

        eQTLdata <- getEQTLdata(lead = mainSNP$variation,
                                Genes = cisGenes,
                                topHits=topHits)

        #pb
        pb$tick()$print()

        #trans eQTLs
        Genes.trans <- Genes[!is.element(Genes$name, cisGenes$name),]

        if(length(Genes.trans) !=0){
          transGenes <- interactionGenes(
            Genes.trans,
            chromIntData)

          transeQTLdata <- getEQTLdata(lead = mainSNP$variation,
                                       Genes = transGenes,
                                       topHits = topHits)
        }else{
          transeQTLdata <- data.frame()
        }

        #pb
        pb$tick()$print()

        #Gene expression
        if(length(Genes)==0)
        {
          geneExpression <- data.frame() #if no genes in region
        } else{
          if (length(cisGenes)==0) {
            cisId <- NULL #if no cis genes
          } else {
            cisId <- unique(cisGenes$name)
          }
          if (!exists("transGenes")) {
            transId <- NULL #if no trans genes with interactions
          } else {
            transId <- unique(transGenes$name)
          }
          geneExpression <- getExpression(
            cisGenes = cisId,
            transGenes = transId)
        }

        #phenotype
        pheno <- SNiPERdata$Pheno[is.element(SNiPERdata$Pheno$SNPS,
                                           topHits$variation),]
        #make list of all plot data to return
        allData <- list(
          SNP=mainSNP,
          topHits=topHits,
          LD=LD,
          Exons=Exon,
          chromInt=chromIntData,
          chromatin=ChromatinData,
          TFs=TFdata,
          meQTLs=SNiPERdata$meQTL[is.element(SNiPERdata$meQTL$rsID,
                                           topHits$variation),],
          pQTLs=pQTLs[order(pQTLs$pvalue, decreasing = F),],
          miQTLexp=exp.miQTLs,
          miQTLpred=pred.miQTLs,
          eQTLs=eQTLdata,
          eQTLsTrans=transeQTLdata,
          geneExpr=geneExpression,
          Phenotype=pheno,
          genes=Genes
        )

        #pb
        pb$tick()$print()

        saveRDS(allData, file = filename)
      }
    }
  }
}
