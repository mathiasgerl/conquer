getGWAScata <- function() {
  gwas <- rio::import("http://ftp.ebi.ac.uk/pub/databases/gwas/releases/latest/gwas-catalog-associations.tsv")
  gwas <- gwas[,c("SNPS", "DISEASE/TRAIT", "P-VALUE")]
  gwas <- unique(gwas)
}
