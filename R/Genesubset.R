Genesubset <- function(Genes, Start, Stop) {
  Range <- GRanges(seqnames = unique(Genes@seqnames),
                   ranges = IRanges(start = Start, end = Stop))
  overlap <- findOverlaps(Genes, Range)
  NewGenes <- Genes[overlap@from,]
  return(NewGenes)
}
