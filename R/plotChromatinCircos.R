plotChromatinCircos <- function(bedList, chromatinData, tissue) { #if tissue!=all
  #get tissues to be plotted
  TissueDict <- list("blood"=c("CD34"="BLD.CD34.PC",
                               "CD4_Memory"="BLD.CD4.MPC",
                               "CD4_Naive"="BLD.CD4.NPC",
                               "CD4_T"="BLD.CD4.CD25M.TPC",
                               "CD8_Naive"="BLD.CD8.NPC",
                               "GM12878"="BLD.GM12878",
                               "K562"="BLD.K562.CNCR"),
                     "fetus"=c("IPS19.11"="IPSC.DF.19.11",
                               "IPS6.9"="IPSC.DF.6.9",
                               "HUVEC"="VAS.HUVEC", "H1ESC"="ESC.H1",
                               "H1_derived_mesenchymal_stem_cell"="ESDR.H1.MSC",
                               "H1_derived_neural_progenitors"="ESDR.H1.NEUR.PROG"),
                     "lung"=c("A549"="LNG.A549.ETOH002.CNCR",
                              "IMR90"="LNG.IMR90", "NHLF"="LNG.NHLF"),
                     "skin"=c("Foreskin_fibroblast"="SKIN.PEN.FRSK.FIB.01",
                              "Foreskin_keratinocyte"="SKIN.PEN.FRSK.KER.02",
                              "Foreskin_melanocyte"="SKIN.PEN.FRSK.MEL.03",
                              "NHEK"="SKIN.NHEK"),
                     "breast"=c("HMEC"="BRST.HMEC"),
                     "brain"=c("Brain_hippocampus_middle"="BRN.HIPP.MID"),
                     "liver"=c("HEP"="LIV.ADLT"),
                     "colon"=c("HCT116"="GI.RECT.SM.MUS"),
                     "cervix"=c("HELA"="CRVX.HELAS3.CNCR"),
                     "muscle"=c("HSMM"="MUS.HSMM"),
                     "pancreas"=c("PANC"="PANC"))
  bedTissues <- levels(droplevels(bedList[[1]]$tissue))
  chromatinTissues <- TissueDict[[tissue]]
  plotChromatins <- unname(chromatinTissues[names(chromatinTissues) %in% bedTissues])
  if (length(na.omit(plotChromatins))<1) {return()} #dont plot if no data available
  #get range that will be plotted
  gr.range <- GenomicRanges::GRanges(
    seqnames = bedList[[3]]$chr,
    ranges = IRanges::IRanges(start = bedList[[3]]$start,
                              end = bedList[[3]]$end))
  #find overlaps in plotrange in the right tissue
  TissueData <- chromatinData[is.element(chromatinData$sample, plotChromatins),]
  overlap <- GenomicRanges::findOverlaps(TissueData, gr.range)
  overlap <- TissueData[overlap@from,]
  overlap <- GenomicRanges::as.data.frame(overlap)
  overlap$sample <- droplevels(overlap$sample)
  #make the quiescent/low regions transparent
  overlap$colr <- as.character(overlap$colr)
  overlap[overlap$colr=="#ffffff", 'colr'] <- NA
  #cut ranges to fit in plot region
  xlims <- circlize::get.cell.meta.data("xlim") #[1] start x value, [2] end x value
  overlap[overlap$start<xlims[1],"start"] <- xlims[1]
  overlap[overlap$end>xlims[2],"end"] <- xlims[2]
  #plot in circle
  circlize::circos.genomicTrackPlotRegion(data=overlap,
                                          ylim = c(0.5, max(as.numeric(overlap$sample))+0.5),
                                          bg.lty=0, track.height=0.03*nlevels(overlap$sample),
                                          track.margin=c(0,0.01), cell.padding=c(0,1,0,1))
  circlize::circos.rect(xleft = overlap$start,
                        ybottom = as.numeric(overlap$sample)-0.4,
                        xright = overlap$end,
                        ytop = as.numeric(overlap$sample)+0.4,
                        col = as.character(overlap$colr),
                        lty=0)
  yradius <- circlize::get.cell.meta.data("yplot")
  yradius <- seq(yradius[1], yradius[2],
                 length.out = nlevels(overlap$sample)+1)
  yradius <- head(yradius, -1)
  text(x=rep(0.2, nlevels(overlap$sample)),
       y=yradius, col = par('col.lab'),
       labels = levels(overlap$sample), cex=0.7, adj = c(1,1))
}
