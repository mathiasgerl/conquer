---
output: github_document
---

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, echo = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

# Single Nucleotide Polymorphism Effects R <img src="man/Figures/CONQUER.png" align="right" width="120" />

*Joey Nap, Gerard Bouland, Emma Schoep, Joline WJ Beulens, Leen ’t Hart and Roderick Slieker*

## Installation

```{r, eval = FALSE}
# Install the the development version from GitLab:  
# install.packages("devtools")
devtools::install_gitlab("rcslieker/SNIPER")
```


## Overview 

With the use of two functions, SNPs are summarised and visualised, namely: `summarise()` and `visualise()`.

* The `summarise()` function is used to collect all data related to SNPs. 
* The `visualise()` function initiates a RStudio Shiny-based dashboard that visualises all relevant plots. 

Note: We use the LD data from the API of NIH. You will need to register on the site to obtain a token. Please see: 

https://ldlink.nci.nih.gov/?tab=apiaccess

The token is send by email and can be provided as character string. 

```{r, message=F, eval=F}
library(SNIPER)

summarise(
  variants = "rs184660829",
  directory = "somedirectory",
  token = "sometoken"
)
```

```{r, eval=F}
visualise(variant = "rs184660829", 
          directory = "somedirectory")
```

&nbsp;
&nbsp;
&nbsp;
&nbsp;

## Data resources:
```{r, echo=F}
temp <- rio::import("Z:/Roderick/001 Projects/062 Public SNIPER/Data resources.xlsx")
temp[is.na(temp)] <- " "
knitr::kable(x = temp)
```